/*
 *  Programming Assignment 3
 *  
 *  Student: William Bodeis
 *  Instructor: Dr. Dharam
 *  Class: CIS301 - Winter 2019
 *  
 *  Version: 1.0
 *  
 *  The two classes in the program are used as a server and client that interact with one another via HTTP type of connection.
 *  
 *  See the included README for more information. 
 *  
 */
package PartB;
import java.io.*;
import java.net.*;
import java.util.*;

public class HttpServerPartB {
    public static void main(String[] args) throws IOException {
        runServer();
    }
    
    private static void runServer() throws IOException {
        // Declaring the port number and using the variable through the code.
        int portNumber = 8080;
        boolean exitProgram = false;
        // The first time that the input is split at the spaces from the input and stored in an array. 
        String[] userInputSplit;
        // ArrayList for storing the text in the file.
        ArrayList<String> fileContents = new ArrayList<>();
        // Storing each line of the HTTP header.
        ArrayList<String> httpHeaderInput = new ArrayList<>();
        // Creating a server socket.
        ServerSocket serverSocket = new ServerSocket(portNumber);
        // Allowing a connection to be made at the server socket.
        Socket clientSocket = serverSocket.accept();
        // Output to the console so show that a connection is made. 
        System.out.println("Connection made with client at port " + portNumber + ".");
        // BefferReader for receiving information from the browser. 
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        // PrintWriter for sending information back to the browser. 
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

        do {
            // Try-catch for the TCP connections being made. 
            try {
                /*
                 *   Storing the browser header and storing it in a String ArrayList.
                 *   The only way I could figure out how to make it work properly was taking the tempHeaderInput String, setting it to the input and checking to make user that is in't null and 0 length.
                 *   It hangs up and won't do anything else it isn't checking for both the null and 0 length.
                 *   We are realistically only using the first line of header, but I wanted to try and figure out how to store every line of it.   
                 */

                httpHeaderInput.add(0,in.readLine());

                userInputSplit = splitUserInput(httpHeaderInput.get(0));

                if (Integer.valueOf(userInputSplit[0]) == 0) {
                    out.println("Server shutdown command accepted.");
                    exitProgram = true;
                }
                else {
                    // Printing to the console what the HTTP message and the cut down version of what you're looking for. 
                    System.out.println("Initial input from the browser: " + httpHeaderInput.get(0));
                    System.out.println("Extracted file name to search for: " + userInputSplit[1]);

                    // Creating a file object and setting it to be the file name from the browser input. 
                    File tempFile = new File(userInputSplit[1]);
                    // Taking the temporary file and first checking to see if it exists or not. 
                    // Checking for it here versus the method call 
                    // TODO More information
                    if (tempFile.exists()) {
                        // HTTP Version and the status code to start the header
                        out.println("HTTP/1.1 200");
                        // TODO - Figure out the rest of the header stuff. 
                        // Retrieving the information from the file that is requested from the user by a method call. 
                        fileContents = getFileContents(userInputSplit[1]);
                        // If-else for printing out the information from the file if it was or was not found. 
                        if (!fileContents.isEmpty()) {
                            for (int i = 0; i < fileContents.size(); i++) {
                                out.println(fileContents.get(i));
                            }
                            out.println("");
                            System.out.println("Response sent to the client.");
                        }
                        else {
                            out.println("There is nothing contained within the text file.");
                            out.println("");
                            System.out.println("There is nothing contained within the text file.");
                        }
                    }
                    else {
                        // HTTP Version and the status code to start the header
                        out.println("HTTP/1.1 404");
                        out.println("No file of the name " + userInputSplit[1] + " exists. \r\n");
                        System.out.println("No file of the name " + userInputSplit[1] + " was found. No response was sent.");
                    }
                    System.out.println();
                    System.out.println("HTTP Header from connection request");
                    System.out.println("___________________________________");
                    System.out.println(httpHeaderInput.get(0));
                    System.out.println();

                }
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        } while (!exitProgram);
        // CLosing out everything so eclipse doesn't yell at you for leaving them open. 
        serverSocket.close();
        clientSocket.close();
        in.close();
        out.close();
        System.out.println("Server shutdown commencing.");
    }

    /*
     *  Method for splitting the first line of the header that is received from the client/browser
     *  It should contain the name of the file that is being searched for within the java project. 
     */
    private static String[] splitUserInput (String stringToSplit) {
        String[] firstSplit = stringToSplit.split("\\s+");
        String[] secondSplit = firstSplit[1].split(",");
        secondSplit[0] = secondSplit[0].substring(1);
        return secondSplit;
    }

    /*
     *  Method for storing the information from a text file in an ArrayList and returning that to the main method. 
     *  Tried to make it dynamic depending on what file is being opened and read. 
     */
    private static ArrayList<String> getFileContents(String fileName) throws IOException {
        ArrayList<String> fileContents = new ArrayList<>();
        File file = new File(fileName);
        Scanner input = new Scanner(file);
        while (input.hasNextLine()) {
            fileContents.add(input.nextLine());
        }
        input.close();
        return fileContents;
    }
}
