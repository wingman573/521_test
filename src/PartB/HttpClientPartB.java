/*
 *  Programming Assignment 3
 *  
 *  Student: William Bodeis
 *  Instructor: Dr. Dharam
 *  Class: CIS301 - Winter 2019
 *  
 *  Version: 1.0
 *  
 *  The two classes in the program are used as a server and client that interact with one another via HTTP type of connection.
 *  
 *  See the included README for more information. 
 *  
 */
package PartB;
import java.net.*;
import java.util.*;
import java.io.*;

public class HttpClientPartB {
    public static void main(String[] args) throws IOException {
        runClient();
    }

    private static void runClient () throws IOException {
        // Just setting the port used here to make it easier to change if I felt like it later. 
        int portNumber = 8080;
        // For storing the user input for when the menu options pop up.
        int menuEntry;
        // Boolean value for the while loop that keeps the program running.
        boolean exitProgram = false;
        // Various strings for the input from the user and output for the server.
        String userInput, tempServerInput, fileNameWithExt, messageToServer = "";
        // Creating a socket to conenct to the server.
        Socket socket = new Socket("localhost",portNumber);
        // Scanner for the user's input.
        Scanner reader = new Scanner(System.in);
        // For sending information to the server. 
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        // For receiving information from the server. 
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        System.out.println("Welcome to the super duper fun file lookup!");
        do {
            /*  
             *  Mini-menu for having the user enter data.
             *  0 exits the program
             *  1 will continue running the program and store the user's input information. 
             */
            System.out.println("To exit the program, enter 0.");
            System.out.println("To look up a file and its contents, enter 1.");
            menuEntry = verifyMenuEntry();
            // Switch statements for the menu entry.
            switch (menuEntry) {
            case 0:  // For exiting the program.
                messageToServer = ("0,0");
                exitProgram = true;
                break;
            case 1:  // For a file.
                System.out.println("Enter the name of the file that you are wanting to look up: ");
                userInput = reader.next();
                fileNameWithExt = checkFileExtension(userInput);
                // Combining the 1 (continue running command), and encrypted names into a single string to send to the server.
                messageToServer = ("1," + fileNameWithExt);
                break;
            }
            try {
                // Does not need the \r\n since it is already being printed as a line.
                // Would otherwise cause the server to run through the loop twice. 
                out.println("GET /" + messageToServer + " HTTP/1.1");
                if (menuEntry == 0) {
                    System.out.println(in.readLine());
                }
                else {
                    System.out.println("Header:");
                    // Header received from the server
                    // 202 for if the file is found, 
                    // or 404 if the file doesn't exist. 
                    System.out.println(in.readLine());
                    System.out.println("Body of the text file:");
                    while ((tempServerInput = in.readLine()) != null && tempServerInput.length() != 0) {
                        System.out.println(tempServerInput);
                    }
                }
                // Adding a space between loops. 
                System.out.println();

            }
            catch(Exception E) {
                E.printStackTrace();
            }
        } while (!exitProgram);
        System.out.println("Have a good day!");
        // CLosing out everything so eclipse doesn't yell at you for leaving them open. 
        socket.close();
        reader.close();
        in.close();
        out.close();
    }

    /* 
     *  Method to be called for checking the name for the file being searched for.
     *  Checking only for text files in the folder.
     *  Method will add ".txt" to the String if it doesn't already have it. 
     *  
     *  First, it gets the length of the String and stores it as an int. 
     *  Then subtracts 4 from the length of the name to start a substring. 
     *  The substring should be the file extension that is being looked for. 
     *  Chose to check the String versus a .split() method for the chance that there was a period or other character in the file name. 
     *  
     */ 
    private static String checkFileExtension (String input) { 
        int stringLength = input.length();
        int startSubstring = stringLength - 4;
        String fileExtension = input.substring(startSubstring);
        if (!fileExtension.equals(".txt")) {
            return input + ".txt";
        }
        else {
            return input;
        } 
    }

    /* 
     *  Checks to see if a valid integer value was entered before saving it to a variable.
     *  Reusing previous code from PA2 assignments for verifying the entry of an integer value.
     */
    private static int verifyMenuEntry () {
        Scanner reader = new Scanner(System.in);
        int tempInteger = 0;
        boolean variableCheck = false, entryCheck = false;
        do {
            // Catch for if a non-numeric or double is entered.
            try {
                do {
                    // Reading the input as an integer.
                    tempInteger = reader.nextInt();
                    // Taking the input and checking that it meets the given criteria.
                    if (tempInteger >= 0 & tempInteger <= 1) {
                        variableCheck = true;
                    }
                    else {
                        System.out.println("Invalid value. Please try again.");
                    }
                } while (!variableCheck);
                entryCheck = true;
            }
            catch (InputMismatchException ex) {
                System.out.println("Non-numeric or invalid number entered. Please try again.");
                // Resetting the Scanner.
                reader.next();
            }
        } while (!entryCheck);
        return tempInteger;
    }
}